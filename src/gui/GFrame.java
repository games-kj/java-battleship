/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import logics.Board;
import logics.Ship;

/**
 *
 * @author Johannes
 */

// The frame that includes both boards, while one board (the one of the GUI user) is open (i.e. ships are visible) and the other one is hidden (i.e. only shots and their results are visible)
public class GFrame extends JFrame {
    
    Board logicBoardHidden;
    Board logicBoardVisible;
    GBoard boardHidden;
    GBoard boardVisible;
    
    public GFrame(Board logicBoardVisible, Board logicBoardHidden) {
        
        this.logicBoardHidden = logicBoardHidden;
        this.logicBoardVisible = logicBoardVisible;
        this.boardHidden = new GBoard(logicBoardHidden, false);
        this.boardVisible = new GBoard(logicBoardVisible, true);
        
        this.setLayout(new BorderLayout());
        this.boardHidden.setPreferredSize(new Dimension(this.boardHidden.fieldsize * this.logicBoardHidden.width + 1, this.boardHidden.fieldsize * (this.logicBoardHidden.height + 1) + 1));
        this.boardVisible.setPreferredSize(new Dimension(this.boardVisible.fieldsize * this.logicBoardVisible.width + 1, this.boardVisible.fieldsize * this.logicBoardVisible.height + 2));
        this.add(this.boardHidden, BorderLayout.NORTH);
        this.add(this.boardVisible, BorderLayout.SOUTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        
        this.boardHidden.repaint();
        this.boardVisible.repaint();
    }
    
    public void close() {
        
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }
    
    public void update() {
        
        this.repaint();
    }
}
