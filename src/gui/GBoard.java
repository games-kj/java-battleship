/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import logics.Board;
import logics.Ship;
import logics.Shot;

/**
 *
 * @author Johannes
 */

// Graphical representation of one player's board
public class GBoard extends JPanel {
    
    public int fieldsize = 30;
    public Board logicBoard;
    public boolean visible;
    
    public GBoard(Board logicBoard, boolean visible) {
        
        this.logicBoard = logicBoard;
        this.visible = visible;
    }

    @Override
    public void paint(Graphics g) {
        
        Graphics2D g2d = (Graphics2D) g;
        
        g.setColor(Color.DARK_GRAY);
        
        g2d.drawRect(0, 0, this.logicBoard.width * this.fieldsize, this.logicBoard.height * this.fieldsize);
        
        for (int y = 1; y < this.logicBoard.height; y++) {
            g2d.drawLine(0, y * this.fieldsize, this.logicBoard.width * this.fieldsize, y * this.fieldsize);
        }
        
        for (int x = 1; x < this.logicBoard.width; x++) {
            g2d.drawLine(x * this.fieldsize, 0, x * this.fieldsize,  this.logicBoard.width * this.fieldsize);
        }
        
        int rectX;
        int rectY;
        int rectWidth;
        int rectHeight;
        
        if (this.visible || true) {
            g.setColor(Color.green);
            for (Ship ship : this.logicBoard.ships) {

                rectX = ship.x * this.fieldsize + 2;
                rectY = ship.y * this.fieldsize + 2;
                if (ship.vertical) {
                    rectWidth = this.fieldsize - 3;
                    rectHeight = ship.size * this.fieldsize - 3;
                }
                else {
                    rectWidth = ship.size * this.fieldsize - 3;
                    rectHeight = this.fieldsize - 3;
                }

                g2d.fillRoundRect(rectX, rectY, rectWidth, rectHeight, (int)(this.fieldsize * 0.6), (int)(this.fieldsize * 0.6));
            }
        }
        
        g.setColor(Color.red);
        for (Ship ship : this.logicBoard.shipsDestroyed) {

            rectX = ship.x * this.fieldsize;
            rectY = ship.y * this.fieldsize;
            if (ship.vertical) {
                rectWidth = this.fieldsize;
                rectHeight = ship.size * this.fieldsize;
            }
            else {
                rectWidth = ship.size * this.fieldsize;
                rectHeight = this.fieldsize;
            }

            g2d.fillRoundRect(rectX, rectY, rectWidth, rectHeight, this.fieldsize, this.fieldsize);
        }
        
        g2d.setStroke(new BasicStroke(4));
        for (Shot shot : this.logicBoard.shotsOnSelf) {
            if (!shot.hit) {
                g.setColor(Color.BLUE);
            }
            else if (shot.destroyed == null) {
                g.setColor(Color.RED);
            }
            else {
                g.setColor(Color.BLACK);
            }
            g2d.drawLine(shot.x * this.fieldsize + 4, shot.y * this.fieldsize + 4, (shot.x + 1) * this.fieldsize - 4, (shot.y + 1) * this.fieldsize - 4);
            g2d.drawLine(shot.x * this.fieldsize + 4, (shot.y + 1) * this.fieldsize - 4, (shot.x + 1) * this.fieldsize -4, shot.y * this.fieldsize + 4);
        }
    }
}
