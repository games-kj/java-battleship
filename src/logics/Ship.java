/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logics;

/**
 *
 * @author Johannes
 */

// Logic representation of a ship with bottom-left point (bottom point for vertical placement, left point for horizontal placement) (x,y)
// TODO: Make 'type' and its relation to 'size' more clear. 'type' is currently just a number between 1 and 4, while these borders are set by 'fleet' in the Board class. E.g. constants might be an option.
public class Ship {
    
    public int type;
    public int x;
    public int y;
    public int size;
    
    public boolean vertical;
    
    public Ship(int type, int x, int y, boolean vertical) {
        
        this.type = type;
        this.x = x;
        this.y = y;
        this.vertical = vertical;
        
        this.size = this.type + 1;
    }
    
}
