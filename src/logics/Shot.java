/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logics;

/**
 *
 * @author Johannes
 */
public class Shot {
    
    public int x;
    public int y;
    public boolean hit;
    public Ship destroyed;
    public Ship hitShip;
    
    public Shot(int x, int y, boolean hit, Ship destroyed) {
        
        this.x = x;
        this.y = y;
        this.hit = hit;
        this.destroyed = destroyed;
    }
    
    public Shot (int x, int y, boolean hit) {
        
        this(x, y, hit, null);
    }
    
    public Shot(int x, int y) {
        
        this(x, y, false);
    }
}
