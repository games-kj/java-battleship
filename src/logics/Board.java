/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logics;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Johannes
 */

// Logic representation of one player's board
// The fields are not explicitly stored, e.g. in a 2D array.
// Instead, there are lists for intact and destroyed ships and for fired and received shots
public class Board {
    
    public int width = 10;
    public int height = 10;
    public List<Ship> ships;
    public List<Ship> shipsDestroyed;
    public int[] fleet = {0, 4, 3, 2, 1};
    public List<Shot> shotsOnSelf;
    public List<Shot> shotsOnEnemy;
    public List<Ship> shipsEnemyDestroyed;
    
    
    public Board() {
        
        this.ships = new ArrayList<Ship>();
        this.shipsDestroyed = new ArrayList<Ship>();
        this.shotsOnSelf = new ArrayList<Shot>();
        this.shotsOnEnemy = new ArrayList<Shot>();
        this.shipsEnemyDestroyed = new ArrayList<Ship>();
    }

    // Place list of ships on the board
    // TODO : Check whether the correct amount of each type of ship (or at least not too many) has been given
    public boolean placeShips(List<Ship> ships) {

        // Ships are not allowed to touch or overlap
        // Therefore, fieldsForbiddenBuffer tracks fields covered by previous ships and their neighbors
        // If all given ships are valid, they are simply added to the board by storing them in the according list
        
        List<Field> fieldsForbiddenBuffer = new ArrayList<Field>();
        
        for (Ship ship : ships) {

            // Vertical ships
            if (ship.vertical) {
                // Add to forbidden: South end neighbor and its two horizontal neighbors (South-East and South-West neighbors of the South end of the ship)
                if (ship.y > 0) {
                    for (int offsetX = -1; offsetX <= 1; offsetX++) {
                        if (ship.x + offsetX >= 0 && ship.x + offsetX < this.width) {
                            fieldsForbiddenBuffer.add(new Field(ship.x + offsetX, ship.y - 1));
                        }
                    }
                }
                // Examine along ship's length
                for (int progressLength = 0; progressLength < ship.size; progressLength++) {
                    // Check if ship contains forbidden field
                    for (Field fieldForbidden : fieldsForbiddenBuffer) {
                        if (ship.x == fieldForbidden.x && ship.y + progressLength == fieldForbidden.y) {
                            return false;
                        }
                    }
                    // Add to forbidden: Ship body and its horizontal neighbors
                    for (int offsetX = -1; offsetX <= 1; offsetX++) {
                        if (ship.x + offsetX >= 0 && ship.x + offsetX < this.width) {
                            fieldsForbiddenBuffer.add(new Field(ship.x + offsetX, ship.y + progressLength));
                        }
                    }
                }
                // Add to forbidden: North end neighbor and its two horizontal neighbors (North-East and North-West neighbors of the North end of the ship)
                if (ship.y + ship.size < this.height) {
                    for (int offsetX = -1; offsetX <= 1; offsetX++) {
                        if (ship.x + offsetX >= 0 && ship.x + offsetX < this.width) {
                            fieldsForbiddenBuffer.add(new Field(ship.x + offsetX, ship.y + ship.size));
                        }
                    }
                }
            }
            // Horizontal ships (analogous to vertical case)
            else {
                if (ship.x > 0) {
                    for (int offsetY = -1; offsetY <= 1; offsetY++) {
                        if (ship.y + offsetY >= 0 && ship.y + offsetY < this.height) {
                            fieldsForbiddenBuffer.add(new Field(ship.x - 1, ship.y + offsetY));
                        }
                    }
                }
                for (int progressLength = 0; progressLength < ship.size; progressLength++) {
                    for (Field fieldForbidden : fieldsForbiddenBuffer) {
                        if (ship.x + progressLength == fieldForbidden.x && ship.y == fieldForbidden.y) {
                            return false;
                        }
                    }
                    for (int offsetY = -1; offsetY <= 1; offsetY++) {
                        if (ship.y + offsetY >= 0 && ship.y + offsetY < this.height) {
                            fieldsForbiddenBuffer.add(new Field(ship.x + progressLength, ship.y + offsetY));
                        }
                    }
                }
                if (ship.x + ship.size < this.width) {
                    for (int offsetY = -1; offsetY <= 1; offsetY++) {
                        if (ship.y + offsetY >= 0 && ship.y + offsetY < this.height) {
                            fieldsForbiddenBuffer.add(new Field(ship.x + ship.size, ship.y + offsetY));
                        }
                    }
                }
            }
        }

        // All checks were successful => Add ships
        this.ships.clear();
        for (Ship ship : ships) {
            this.ships.add(new Ship(ship.type, ship.x, ship.y, ship.vertical));
        }
        return true;
    }
    
    public void clearShips() {
        
        this.ships.clear();
    }

    // Register shot by enemy on own territory
    public Shot takeShot(Shot shot) {
        
        Shot internal = new Shot(shot.x, shot.y);

        // Check if same shot has been done before
        for (Shot onSelf : this.shotsOnSelf) {
            if (shot.x == onSelf.x && shot.y == onSelf.y) {
                return null;
            }
        }
        
        this.shotsOnSelf.add(internal);

        // Check if a ship was hit
        for (Ship ship : this.ships) {
            if ((ship.vertical && shot.x == ship.x && shot.y >= ship.y && shot.y < ship.y + ship.size) || (!ship.vertical && shot.x >= ship.x && shot.x < ship.x + ship.size && shot.y == ship.y)) {
                internal.hit = true;
                internal.hitShip = ship;
                // Check if hit destroys the ship (i.e. if it hit all fields of the ship)
                int hits = 0;
                for (Shot onSelf : this.shotsOnSelf) {
                    if (onSelf.hitShip == ship) {
                        hits++;
                    }
                }
                if (hits >= ship.size) {
                    internal.destroyed = ship;
                }
                break;
            }
        }

        // If a ship was destroyed: Update lists of ships and shots
        if (internal.destroyed != null) {
            this.shipsDestroyed.add(internal.hitShip);
            this.ships.remove(internal.hitShip);
            for (Shot onSelf : this.shotsOnSelf) {
                if (onSelf.hitShip == internal.hitShip) {
                    onSelf.destroyed = onSelf.hitShip;
                }
            }
        }

        // Return new shot with reference to destroyed ship
        Shot res = new Shot(internal.x, internal.y, internal.hit, internal.destroyed);

        return res;
    }

    // After firing a shot on enemy territory, the enemy has to disclose if it was a hit and if a ship was destroyed.
    // This function processes the resulting info
    public void adjustEnemyBoard(Shot shot) {
        
        this.shotsOnEnemy.add(shot);

        // If an enemy ship was destroyed: Update info of involved previous shots
        if (shot.destroyed != null) {
            this.shipsEnemyDestroyed.add(shot.destroyed);
            
            if (shot.destroyed.vertical) {
                for (int progress = 0; progress < shot.destroyed.size; progress++) {
                    for (Shot onEnemy : this.shotsOnEnemy) {
                        if (onEnemy.x == shot.destroyed.x && onEnemy.y == shot.destroyed.y + progress) {
                            onEnemy.destroyed = shot.destroyed;
                            onEnemy.hitShip = shot.destroyed;
                            break;
                        }
                    }
                }
            }
            else {
                for (int progress = 0; progress < shot.destroyed.size; progress++) {
                    for (Shot onEnemy : this.shotsOnEnemy) {
                        if (onEnemy.x == shot.destroyed.x + progress && onEnemy.y == shot.destroyed.y) {
                            onEnemy.destroyed = shot.destroyed;
                            onEnemy.hitShip = shot.destroyed;
                            break;
                        }
                    }
                }
            }
        }
    }
}
