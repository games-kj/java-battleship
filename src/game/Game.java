/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import agents.Agent;
import agents.AgentConcept1;
import agents.AgentRandom;
import agents.Human;
import gui.GFrame;
import java.util.List;
import logics.Board;
import logics.Ship;
import logics.Shot;

/**
 *
 * @author Johannes
 */

// Represents a game of battleship, letting two agents compete and managing a GUI
public class Game {
    
    public GFrame gui;
    public Board board1;
    public Board board2;
    
    public Agent player1;
    public Agent player2;
    public int turn = 0;
    public int winner = -1;
    
    public Game(Agent player1, Agent player2) {
        this.player1 = player1;
        this.player2 = player2;
    }
    
    public void start() {
        
        if (this.gui != null) {
            this.gui.close();
        }
        
        this.board1 = new Board();
        this.board2 = new Board();

        // Players get separate boards
        this.player1.init(board1);
        this.player2.init(board2);
        
        this.gui = new GFrame(this.board1, this.board2);
        this.gui.update();
        
        List<Ship> ships1 = player1.setShips();
        board1.placeShips(ships1);
        List<Ship> ships2 = player2.setShips();
        board2.placeShips(ships2);
        
        this.gui.update();
        
        while(true) {
            this.doTurn();
            this.gui.update();
            if (this.board1.ships.isEmpty() || this.board2.ships.isEmpty()) {
                break;
            }
        }
        if (this.board1.ships.isEmpty()) {
            System.out.println("WINNER: player1");
        }
        else {
            System.out.println("WINNER: player0");
        }
    }
    
    public void doTurn() {
        
        Shot shot;
        Shot result;
        
        while(true) {
            shot = (turn == 0 ? player1.chooseMove() : player2.chooseMove());
            result = (turn == 0 ? board2.takeShot(shot) : board1.takeShot(shot));
            if (result != null) {
                if (turn == 0) {
                    board1.adjustEnemyBoard(result);
                }
                else {
                    board2.adjustEnemyBoard(result);
                }
                break;
            }
            System.out.println("Board rejected the move (" + shot.x + "/" + shot.y + ")");
        }
        
        this.turn ^= 1;
    }

    // If you use this as the project's main function, you can set which players will compete here
    public static void main(String[] args) {
        
        Agent player1 = new Human();
        Agent player2 = new AgentConcept1();
        
        Game game = new Game(player1, player2);
        
        game.start();
    }
}
