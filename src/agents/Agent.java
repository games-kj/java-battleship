/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import java.util.ArrayList;
import java.util.List;
import logics.Board;
import logics.Field;
import logics.Ship;
import logics.Shot;

/**
 *
 * @author Johannes
 */

// Allows for different agents (e.g. human, computer with various strategies, ...) to be used in a Game or in the api
public class Agent {
    
    public Board board;
    
    public void init(Board board) {
        
        this.board = board;
    }

    // Choose a move to do next
    public Shot chooseMove() {
        
        Shot res = new Shot(0, 0);
        
        return res;
    }

    // Place ships on the board
    public List<Ship> setShips() {
        
        List<Ship> res = new ArrayList<Ship>();
        
        return res;
    }
}
