/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import logics.Board;
import logics.Field;
import logics.Ship;
import logics.Shot;

/**
 *
 * @author Johannes
 */
public class AgentConcept1 extends Agent {
    
    @Override
    public Shot chooseMove() {
        
        System.out.println("Starting calculations");
        
        try {
            TimeUnit.MILLISECONDS.sleep(1000);
            
        } catch (InterruptedException ex) {
            Logger.getLogger(AgentRandom.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Shot res = new Shot(0, 0);
        
        List<Field> forbiddenFields = this.calculateForbiddenFields();
        List<Shot> openShots = this.calculateOpenShots();
        Ship smallestEnemyShip = this.smallestEnemyShip();
        
        Shot followUp = this.followOnOpenShot(openShots, forbiddenFields);
        if (followUp != null) return followUp;
        
        Random rand = new Random();
        
        int possibleFields = this.board.width * this.board.height;
        int fieldStart = rand.nextInt(possibleFields);
        int currentShotNr;
        Shot currentShot = new Shot(0, 0);
        int possibilitiesVertical;
        int possibilitiesHorizontal;
        int foundBest = possibleFields * 2;
        boolean positionPossible;
        for (int progressShot = 0; progressShot < possibleFields; progressShot++) {
            currentShotNr = (fieldStart + progressShot) % possibleFields;
            currentShot.x = currentShotNr % this.board.width;
            currentShot.y = currentShotNr / this.board.width;
            if (!this.isReasonableShot(currentShot, forbiddenFields)) {
                continue;
            }
            possibilitiesVertical = 0;
            possibilitiesHorizontal = 0;
            for (int shipX = 0; shipX < this.board.width; shipX++) {
                for (int shipY = 0; shipY < this.board.width - smallestEnemyShip.size; shipY++) {
                    positionPossible = true;
                    for (int progressShip = 0; progressShip < smallestEnemyShip.size; progressShip++) {
                        for (Field field : forbiddenFields) {
                            if (shipX == field.x && shipY + progressShip == field.y) {
                                positionPossible = false;
                                break;
                            }
                        }
                        if (!positionPossible) break;
                        if (shipX == currentShot.x && shipY + progressShip == currentShot.y) {
                            positionPossible = false;
                        }
                        if (!positionPossible) break;
                    }
                    if (positionPossible) possibilitiesVertical++;
                }
            }
            for (int shipX = 0; shipX < this.board.width - smallestEnemyShip.size; shipX++) {
                for (int shipY = 0; shipY < this.board.width; shipY++) {
                    positionPossible = true;
                    for (int progressShip = 0; progressShip < smallestEnemyShip.size; progressShip++) {
                        for (Field field : forbiddenFields) {
                            if (shipX + progressShip == field.x && shipY == field.y) {
                                positionPossible = false;
                                break;
                            }
                        }
                        if (!positionPossible) break;
                        if (shipX + progressShip == currentShot.x && shipY == currentShot.y) {
                            positionPossible = false;
                        }
                        if (!positionPossible) break;
                    }
                    if (positionPossible) possibilitiesHorizontal++;
                }
            }
            if (possibilitiesVertical + possibilitiesHorizontal < foundBest) {
                foundBest = possibilitiesVertical + possibilitiesHorizontal;
                res = new Shot(currentShot.x, currentShot.y);
                System.out.println("--- New best: " + foundBest);
            }
        }
        
        return res;
    }

    // Simple checks if a shot could potentially hit an enemy ship
    public boolean isReasonableShot (Shot shot, List<Field> forbiddenFields) {

        // Check if the shot lies within the board
        if (shot.x < 0 || shot.x >= this.board.width || shot.y < 0 || shot.y >= this.board.height) {
            return false;
        }

        // Check if the same field has been shot at before
        for (Shot onEnemy : this.board.shotsOnEnemy) {
            if (onEnemy.x == shot.x && onEnemy.y == shot.y) {
                return false;
            }
        }

        // Check if the enemy is allowed to place a ship there
        for (Field field : forbiddenFields) {
            if (field.x == shot.x && field.y == shot.y) {
                return false;
            }
        }
        
        return true;
    }

    // Calculate the fields of the board that can not be occupied by enemy ships, e.g. because they are neighbors of known ships
    public List<Field> calculateForbiddenFields() {
        
        List<Field> res = new ArrayList<Field>();
        
        for (Ship ship : this.board.shipsEnemyDestroyed) {
            
            if (ship.vertical) {
                if (ship.y > 0) {
                    for (int offsetX = -1; offsetX <= 1; offsetX++) {
                        if (ship.x + offsetX >= 0 && ship.x + offsetX < this.board.width) {
                            res.add(new Field(ship.x + offsetX, ship.y - 1));
                        }
                    }
                }
                for (int progressLength = 0; progressLength < ship.size; progressLength++) {
                    for (int offsetX = -1; offsetX <= 1; offsetX++) {
                        if (ship.x + offsetX >= 0 && ship.x + offsetX < this.board.width) {
                            res.add(new Field(ship.x + offsetX, ship.y + progressLength));
                        }
                    }
                }
                if (ship.y + ship.size < this.board.height) {
                    for (int offsetX = -1; offsetX <= 1; offsetX++) {
                        if (ship.x + offsetX >= 0 && ship.x + offsetX < this.board.width) {
                            res.add(new Field(ship.x + offsetX, ship.y + ship.size));
                        }
                    }
                }
            }
            else {
                if (ship.x > 0) {
                    for (int offsetY = -1; offsetY <= 1; offsetY++) {
                        if (ship.y + offsetY >= 0 && ship.y + offsetY < this.board.height) {
                            res.add(new Field(ship.x - 1, ship.y + offsetY));
                        }
                    }
                }
                for (int progressLength = 0; progressLength < ship.size; progressLength++) {
                    
                    for (int offsetY = -1; offsetY <= 1; offsetY++) {
                        if (ship.y + offsetY >= 0 && ship.y + offsetY < this.board.height) {
                            res.add(new Field(ship.x + progressLength, ship.y + offsetY));
                        }
                    }
                }
                if (ship.x + ship.size < this.board.width) {
                    for (int offsetY = -1; offsetY <= 1; offsetY++) {
                        if (ship.y + offsetY >= 0 && ship.y + offsetY < this.board.height) {
                            res.add(new Field(ship.x + ship.size, ship.y + offsetY));
                        }
                    }
                }
            }
        }
        
        return res;
    }

    // Shots that have hit but not destroyed an enemy ship
    public List<Shot> calculateOpenShots() {
        
        List<Shot> res = new ArrayList<Shot>();
        
        for (Shot shot : this.board.shotsOnEnemy) {
            if (shot.hit && shot.destroyed == null) {
                res.add(shot);
            }
        }
        
        return res;
    }

    // Tries to follow up on a successful shot, i.e. a shot that hit an enemy shot but did not yet destroy it
    // Returns the first such shot that could actually hit a different field of a hit enemy ship
    public Shot followOnOpenShot(List<Shot> openShots, List<Field> forbiddenFields) {
        
        Shot res = new Shot(0, 0);
        
        boolean foundNeighbour;
        
        for (Shot openShot : openShots) {
            foundNeighbour = false;
            for (Shot openShot2 : openShots) {
                if (openShot.x + 1 == openShot2.x && openShot.y == openShot2.y) {
                    foundNeighbour = true;
                    res.x = openShot.x - 1;
                    res.y = openShot.y;
                    if (this.isReasonableShot(res, forbiddenFields)) {
                        return res;
                    }
                }
                if (openShot.x - 1 == openShot2.x && openShot.y == openShot2.y) {
                    foundNeighbour = true;
                    res.x = openShot.x + 1;
                    res.y = openShot.y;
                    if (this.isReasonableShot(res, forbiddenFields)) {
                        return res;
                    }
                }
                if (openShot.x == openShot2.x && openShot.y + 1 == openShot2.y) {
                    foundNeighbour = true;
                    res.x = openShot.x;
                    res.y = openShot.y - 1;
                    if (this.isReasonableShot(res, forbiddenFields)) {
                        return res;
                    }
                }
                if (openShot.x == openShot2.x && openShot.y - 1 == openShot2.y) {
                    foundNeighbour = true;
                    res.x = openShot.x;
                    res.y = openShot.y + 1;
                    if (this.isReasonableShot(res, forbiddenFields)) {
                        return res;
                    }
                }
            }
            
            if (!foundNeighbour) {
                res.x = openShot.x - 1;
                res.y = openShot.y;
                if (this.isReasonableShot(res, forbiddenFields)) {
                    return res;
                }
                res.x = openShot.x + 1;
                if (this.isReasonableShot(res, forbiddenFields)) {
                    return res;
                }
                res.x = openShot.x;
                res.y = openShot.y - 1;
                if (this.isReasonableShot(res, forbiddenFields)) {
                    return res;
                }
                res.x = openShot.x;
                res.y = openShot.y + 1;
                if (this.isReasonableShot(res, forbiddenFields)) {
                    return res;
                }
            }
        }
        
        return null;
    }

    // Smallest (not yet destroyed) ship the enemy has left in its fleet
    public Ship smallestEnemyShip() {
        
        int[] enemyFleet = this.board.fleet.clone();
        
        for (Ship ship : this.board.shipsEnemyDestroyed) {
            enemyFleet[ship.type]--;
        }
        
        for (int type = 1; type < enemyFleet.length; type++) {
            if (enemyFleet[type] > 0) {
                return new Ship(type, 0, 0, false);
            }
        }
        
        return new Ship(0, 0, 0, false);
    }
    
    @Override
    public List<Ship> setShips() {
        
        List<Ship> prototypes = new ArrayList<Ship>();
        for (int type = 1; type < this.board.fleet.length; type++) {
            for (int instanceNr = 0; instanceNr < this.board.fleet[type]; instanceNr++) {
                prototypes.add(new Ship(type, 0, 0, false));
            }
        }
        
        List<Ship> trial = new ArrayList<Ship>();
        
        Board testboard = new Board();
        Random rand = new Random();
        
        int numPossibleFields;
        int fieldStart;
        int fieldCurrent;
        int linewidth;
        boolean placementSuccessful = false;
        for (int attemptNr = 0; attemptNr < 1000; attemptNr++) {
            trial.clear();
            for (Ship ship : prototypes) {
                trial.add(ship);
                ship.vertical = rand.nextBoolean();
                numPossibleFields = (ship.vertical? this.board.width * (this.board.height - ship.size) : (this.board.width - ship.size) * this.board.height);
                fieldStart = rand.nextInt(numPossibleFields);
                linewidth = (ship.vertical? this.board.width : this.board.width - ship.size);
                placementSuccessful = false;
                for (int triedField = 0; triedField < numPossibleFields; triedField++) {
                    fieldCurrent = (fieldStart + triedField) % numPossibleFields;
                    ship.x = fieldCurrent % linewidth;
                    ship.y = fieldCurrent / linewidth;
                    testboard.clearShips();
                    if (testboard.placeShips(trial)) {
                        placementSuccessful = true;
                        break;
                    }
                }
                if (!placementSuccessful) {
                    break;
                }
            }
            if (placementSuccessful) {
                System.out.println("AgentRandom: Found valid placement of " + trial.size() + " ships at run " + attemptNr);
                return trial;
            }
        }
        
        System.out.println("AgentRandom: Didn't find valid random placement, will use fallback");
        
        int y = 0;
        int x = 0;
        for (Ship ship: prototypes) {
            if (y >= this.board.height) {
                y = 0;
                x += (this.board.width / 2);
            }
            ship.vertical = false;
            ship.x = x;
            ship.y = y;
            y += 2;
        }
        
        if (!testboard.placeShips(prototypes)) {
            System.out.println("==== ERROR (AGENTRANDOM) : FALLBACK SHIP PLACEMENT WAS NOT ACCEPTED =====");
        }
        
        return prototypes;
    }
    
}
