/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import logics.Board;
import logics.Ship;
import logics.Shot;

/**
 *
 * @author Johannes
 */

// Agent that reads the initial ship placement and then next move for each round from a CLI user
public class Human extends Agent {
    
    @Override
    public Shot chooseMove() {
        
        Shot res = new Shot(0, 0);
        
        System.out.println("Enter shot:");
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String readMove = "";
        int shotX;
        int shotY;
        boolean failed;

        // Keep reading moves until a valid one is put in
        while (true) {
            try {
                readMove = reader.readLine();
            } catch (IOException ex) {
                Logger.getLogger(Human.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("===== ERROR (HUMAN) : EXCEPTION WHILE READING INPUT =====");
                continue;
            }

            // Allow uppercase and lowercase x coordinates
            readMove = readMove.toLowerCase();
            
            if (!readMove.matches("^[a-j][0-9][0-9]?$")) {
                System.out.println("Invalid input format, expecting x as character and y as number, e.g. a1 for bottom left corner. Please try again: ");
                continue;
            }
            
            shotX = Character.getNumericValue(readMove.charAt(0)) - 10;
            if (readMove.length() == 2) {
                shotY = Character.getNumericValue(readMove.charAt(1)) - 1;
            }
            else {
                shotY = Integer.parseInt(readMove.substring(1, 3)) - 1;
            }
            
            if (shotX < 0 || shotX >= this.board.width || shotY < 0 || shotY >= this.board.height) {
                System.out.println("Invalid shot coordinates (x=" + shotX + ", y=" + shotY +"), please try again: ");
                continue;
            }
            
            res.x = shotX;
            res.y = shotY;
        
            return res;
        }
    }
    
    @Override
    public List<Ship> setShips() {

        // Expects space-separated list of ships, each in format <shiptype><startx><starty><h for horizontal or v for vertical>
        //  where shiptype is shiplength - 1, so 1 for patrol boat, 2 for submarine or destroyer, 3 for battleship and 4 for carrier.
        // Example: 1a1v 1c1v 1e1v 1g1v 2i1v 2a4h 2a6h 3a8h 3a10h 4j6v
        
        List<Ship> res = new ArrayList<Ship>();
        
        System.out.println("Enter ship placement: ");
            
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String readMove = "";
        Board testboard = new Board();
        int[] fleetCopy;
        String[] shipsText;
        int shipT;
        int shipX;
        int shipY;
        boolean shipV;
        boolean failed;
        
        while (true) {
            try {
                readMove = reader.readLine();
            } catch (IOException ex) {
                Logger.getLogger(Human.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("===== ERROR (HUMAN) : EXCEPTION WHILE READING INPUT =====");
                continue;
            }
            
            if (readMove.charAt(readMove.length() - 1) != ' ') readMove += " ";
            readMove = readMove.toLowerCase();
            
            if (!readMove.matches("^([1-4][a-j][0-9][0-9]?[hv] )+$")) {
                System.out.println("Invalid input format, please try again: ");
                continue;
            }
            
            shipsText = readMove.split(" ");
            fleetCopy = this.board.fleet.clone();
            
            res.clear();
            failed = false;
            
            for (int i = 0; i < shipsText.length; i++) {
                if (shipsText[i].length() < 1) continue;
                shipT = Character.getNumericValue(shipsText[i].charAt(0));
                shipX = Character.getNumericValue(shipsText[i].charAt(1)) - 10;
                if (Character.getNumericValue(shipsText[i].charAt(3)) > 9) {
                    shipY = Character.getNumericValue(shipsText[i].charAt(2)) - 1;
                    shipV = (shipsText[i].charAt(3) == 'v');
                }
                else {
                    shipY = Integer.parseInt(shipsText[i].substring(2, 4)) - 1;
                    shipV = (shipsText[i].charAt(4) == 'v');
                }
                if (shipT > fleetCopy.length - 1 || shipX < 0 || shipY >= this.board.width || shipY < 0 || shipY >= this.board.height) {
                    failed = true;
                    System.out.println("Invalid ship types or coordinates (t=" + shipT + ", x=" + shipX + ", y=" + shipY + "), please try again: ");
                    break;
                }
                res.add(new Ship(shipT, shipX, shipY, shipV));
                fleetCopy[shipT] --;
            }
            
            if (failed) continue;
            
            for (int numShipsLeft : fleetCopy) {
                if (numShipsLeft != 0) {
                    failed = true;
                    System.out.println("Invalid distribution of types, please try again: ");
                    break;
                }
            }
            
            if (failed) continue;
            
            if (!testboard.placeShips(res)) {
                System.out.println("Board would reject this placement, please try again: ");
                continue;
            }
            
            return res;
        }
    }
}
