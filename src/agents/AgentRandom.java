/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import logics.Board;
import logics.Field;
import logics.Ship;
import logics.Shot;

/**
 *
 * @author Johannes
 */

// Simplistic Agent just for testing
public class AgentRandom extends Agent {
    
    @Override
    public Shot chooseMove() {
        
        try {
            TimeUnit.MILLISECONDS.sleep(1000);
            
        } catch (InterruptedException ex) {
            Logger.getLogger(AgentRandom.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Shot res;
        Random rand = new Random();
        boolean isValid;
        
        while (true) {
            res = new Shot(rand.nextInt(this.board.width), rand.nextInt(this.board.height));
            isValid = true;
            for (Shot shot : this.board.shotsOnEnemy) {
                if (shot.x == res.x && shot.y == res.y) {
                    isValid = false;
                    break;
                }
            }
            if (isValid) {
                return res;
            }
        }
    }
    
    @Override
    public List<Ship> setShips() {
        
        return this.calculateRandomSetting2();
    }
    
    public List<Ship> calculateRandomSetting1() {
        
        List<Ship> res = new ArrayList<Ship>();
        for (int type = 1; type < this.board.fleet.length; type++) {
            for (int instanceNr = 0; instanceNr < this.board.fleet[type]; instanceNr++) {
                res.add(new Ship(type, 0, 0, false));
            }
        }
        
        Board testboard = new Board();
        Random rand = new Random();
        
        for (int attemptNr = 0; attemptNr < 100000000; attemptNr ++) {
            
            for (Ship ship : res) {
                ship.vertical = rand.nextBoolean();
                if (ship.vertical) {
                    ship.x = rand.nextInt(this.board.width);
                    ship.y = rand.nextInt(this.board.height - ship.size);
                }
                else {
                    ship.x = rand.nextInt(this.board.width - ship.size);
                    ship.y = rand.nextInt(this.board.height);
                }
            }
            
            if (testboard.placeShips(res)) {
                System.out.println("AgentRandom: Found valid placement at run " + attemptNr);
                return res;
            }
        }
        
        System.out.println("AgentRandom: Didn't find valid random placement, will use fallback");
        
        int y = 0;
        int x = 0;
        for (Ship ship: res) {
            if (y >= this.board.height) {
                y = 0;
                x += (this.board.width / 2);
            }
            ship.vertical = false;
            ship.x = x;
            ship.y = y;
            y += 2;
        }
        
        if (!testboard.placeShips(res)) {
            System.out.println("==== ERROR (AGENTRANDOM) : FALLBACK SHIP PLACEMENT WAS NOT ACCEPTED =====");
        }
        
        return res;
    }
    
    public List<Ship> calculateRandomSetting2() {
        
        List<Ship> prototypes = new ArrayList<Ship>();
        for (int type = 1; type < this.board.fleet.length; type++) {
            for (int instanceNr = 0; instanceNr < this.board.fleet[type]; instanceNr++) {
                prototypes.add(new Ship(type, 0, 0, false));
            }
        }
        
        List<Ship> trial = new ArrayList<Ship>();
        
        Board testboard = new Board();
        Random rand = new Random();
        
        int numPossibleFields;
        int fieldStart;
        int fieldCurrent;
        int linewidth;
        boolean placementSuccessful = false;
        for (int attemptNr = 0; attemptNr < 1000; attemptNr++) {
            trial.clear();
            for (Ship ship : prototypes) {
                trial.add(ship);
                ship.vertical = rand.nextBoolean();
                numPossibleFields = (ship.vertical? this.board.width * (this.board.height - ship.size) : (this.board.width - ship.size) * this.board.height);
                fieldStart = rand.nextInt(numPossibleFields);
                linewidth = (ship.vertical? this.board.width : this.board.width - ship.size);
                placementSuccessful = false;
                for (int triedField = 0; triedField < numPossibleFields; triedField++) {
                    fieldCurrent = (fieldStart + triedField) % numPossibleFields;
                    ship.x = fieldCurrent % linewidth;
                    ship.y = fieldCurrent / linewidth;
                    testboard.clearShips();
                    if (testboard.placeShips(trial)) {
                        placementSuccessful = true;
                        break;
                    }
                }
                if (!placementSuccessful) {
                    break;
                }
            }
            if (placementSuccessful) {
                System.out.println("AgentRandom: Found valid placement of " + trial.size() + " ships at run " + attemptNr);
                return trial;
            }
        }
        
        System.out.println("AgentRandom: Didn't find valid random placement, will use fallback");
        
        int y = 0;
        int x = 0;
        for (Ship ship: prototypes) {
            if (y >= this.board.height) {
                y = 0;
                x += (this.board.width / 2);
            }
            ship.vertical = false;
            ship.x = x;
            ship.y = y;
            y += 2;
        }
        
        if (!testboard.placeShips(prototypes)) {
            System.out.println("==== ERROR (AGENTRANDOM) : FALLBACK SHIP PLACEMENT WAS NOT ACCEPTED =====");
        }
        
        return prototypes;
    }
}
