# java-battleship

This is a java implementation of the classic game of battleship. The usecase-agnostic approach revolves about the general rules, some infrastructure and agents.

The fastest way to reach a playable game is using the `main` function of the `Game` class. However, the human agent is currently barely usable as the placement of the ships is tedious and poorly documented.

The code is at the state of the first working solution, i.e. many optimizations can be done, such as adjusting the variable scopes.